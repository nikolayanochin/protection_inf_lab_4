<?php
/**
 * Created with love by АльянсЭкспресс.
 * Author: Anochin Nikolay
 * Email: titan12345@mail.ru
 * Phone: +7 925 174 1314
 * Date: 08.11.2018
 * Time: 18:46
 */


namespace lib;

/**
 * Class Client
 * @package lib
 */
class Client extends ClientHelper
{
    private $name;
    private $secret_key;
    private $public_key;

    private $client_public_key;
    private $message_from_client;


    public function __construct($name)
    {
        $this->name = $name;
    }


    public function generateKeys()
    {
        $p = gmp_nextprime($this->getBigRandomIntString(100));
        $q = gmp_nextprime($this->getBigRandomIntString(110));

        $n = gmp_strval(gmp_mul($p, $q));

        $func = gmp_mul(gmp_sub($p, "1"), gmp_sub($q, "1"));
        $e = "65537";

        $d = gmp_strval($this->inv($e, $func));

        $this->secret_key = [$d, $n];
        $this->public_key = [$e, $n];
        if($d == 0) {
            $this->generateKeys();
        }
    }

    private function inv($x, $m)
    {
        $u = [$x, 1];
        $v = [$m, 0];
        while ($v[0] != 0) {
            $q = gmp_div_qr($u[0], $v[0])[0];
            $t = [gmp_mod($u[0], $v[0]), gmp_sub($u[1], gmp_mul($q, $v[1]))];
            $u = $v;
            $v = $t;
        }
        if ($u[0] != 1) return 0;
        return gmp_mod($u[1], $m);
    }

    public function setClientPublicKey($key)
    {
        $this->client_public_key = $key;
    }

    public function sendPublicKey(Client $client)
    {
        $client->setClientPublicKey($this->public_key);
    }

    public function sendMessage($message, Client $client)
    {
        $intMess = $this->convertStringToInt($message);
        $encode = $this->encodeMessage($intMess);
        echo "{$this->name} зашифровал сообщение: {$message} в {$encode} \n";
        $client->getMessage($encode);
        $client->decodeMessage();
        echo "--------------------------------\n\n";
    }

    private function encodeMessage($message)
    {
        return gmp_mod(gmp_pow($message, $this->client_public_key[0]), $this->client_public_key[1]);
    }

    public function getMessage($message)
    {
        echo "{$this->name} получил сообщение: {$message} \n";
        $this->message_from_client = $message;
    }

    private function decodeMessage()
    {
        $decoded = gmp_strval(gmp_powm($this->message_from_client, $this->secret_key[0], $this->secret_key[1]));
        $decoded = $this->convertIntToString($decoded);
        echo "{$this->name} расшифровал сообщение: {$decoded} \n";
    }

    private function getBigRandomIntString($len = 1)
    {
        $str = 1;
        while ($len--) {
            $str .= rand(0, 9);
        }
        return $str;
    }

}