<?php
/**
 * Created with love by АльянсЭкспресс.
 * Author: Anochin Nikolay
 * Email: titan12345@mail.ru
 * Phone: +7 925 174 1314
 * Date: 08.11.2018
 * Time: 18:50
 */


namespace lib;


class ClientHelper
{
    protected function convertStringToInt($string)
    {
        $code = "";
        for ($i = 0; $i < strlen($string); $i++) {
            $code .= str_pad(ord($string[$i]), 3, "3", STR_PAD_LEFT);
        }
        return $code;
    }

    protected function convertIntToString($intStr)
    {
        $code = "";
        for ($i = 0; $i < strlen($intStr); $i += 3) {
            $first = $intStr[$i] == 3 ? 0 : $intStr[$i];
            $code .=  chr((int)"{$first}{$intStr[$i+1]}{$intStr[$i+2]}");
        }
        return $code;
    }
}