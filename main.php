<?php
/**
 * Created with love by АльянсЭкспресс.
 * Author: Anochin Nikolay
 * Email: titan12345@mail.ru
 * Phone: +7 925 174 1314
 * Date: 11.10.2018
 * Time: 16:34
 */
ini_set('memory_limit', '4024M');
require_once('autoload.php');

use lib\Client;

$client_1 = new Client('Коля');
$client_2 = new Client('Вася');


$client_1->generateKeys();
$client_2->generateKeys();

// Обмен публичными ключами
$client_1->sendPublicKey($client_2);
$client_2->sendPublicKey($client_1);

$client_1->sendMessage("Привет, Вася! :)", $client_2);
$client_2->sendMessage("Привет, Коля! :)", $client_1);


